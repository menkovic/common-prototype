package org.nmenkovic.microservices.common;

import org.nmenkovic.microservices.common.dto.DTO;


public class Message {

    private DTO dto;
    private String operation;

    public Message() {
    }

    public Message(DTO dto, String operation) {
        this.dto = dto;
        this.operation = operation;
    }

    public DTO getDto() {
        return dto;
    }

    public void setDto(DTO dto) {
        this.dto = dto;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (dto != null ? !dto.equals(message.dto) : message.dto != null) return false;
        return operation != null ? operation.equals(message.operation) : message.operation == null;

    }

    @Override
    public int hashCode() {
        int result = dto != null ? dto.hashCode() : 0;
        result = 31 * result + (operation != null ? operation.hashCode() : 0);
        return result;
    }
}
